const router = require('express').Router();
const Definition = require('../models/definition');

// GET all Definitions by user
router.get('/', (req, res) => {
    Definition.find({ owner: req.user }).then((definitions) => {
        res.json(definitions);
    });
});

// POST a new definition
router.post('/', (req, res) => {
    const def = new Definition({
        owner: req.user,
        logType: req.body.definition.type,
        description: req.body.definition.description
    });

    def.save().then((definition) => {
        res.json({
            message: 'saved!',
            definition: definition
        });
    });
});

// UPDATE an existing definition
router.put('/:id', (req, res) => {
    Definition.findOne({ _id: req.params.id, owner: req.user })
    .then((definition) => {
        definition.logType = req.body.definition.type,
        definition.description = request.body.definition.description;

        definition.save().then((definition) => {
            res.json({
                message: 'updated!',
                definition: definition
            })
        });
    });
});

// DELETE a definition
router.delete('/:id', (req, res) => {
    Definition.findOne({ _id: req.params.id, owner: req.user})
    .then((definition) => {
        definition.remove().then( () => {
            res.json({
                message: 'deleted',
                definition: definition
            });
        });
    });
});

module.exports = router;