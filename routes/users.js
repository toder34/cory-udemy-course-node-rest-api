const router = require('express').Router();
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const constants = require('../config/constants');

router.post('/', (req, res) => {
    const user = new User({
        username: req.body.user.username,
        email: req.body.user.email,
        passhash: bcrypt.hashSync(req.body.user.pwd, 10)
    });

    user.save().then(
        (newuser) => {
            const sessionToken = jwt.sign({ id: user._id}, constants.JWT_SECRET, { expiresIn: 60 * 60 * 24 });
            res.json({
                user: newuser,
                message: 'success',
                sessionToken: sessionToken
            });
        },
        (err) => {
            res.status(500, err.message);
        }
    );
});

module.exports = router;