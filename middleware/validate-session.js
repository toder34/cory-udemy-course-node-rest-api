var jwt = require('jsonwebtoken');
var User = require('../models/user');
var constants = require('../config/constants');
module.exports = (req, res, next) => {
    var sessionToken = req.headers.authorization;
    
    console.log(constants.JWT_SECRET);
    if (!req.body.user && sessionToken) {
        //jwt check 
        jwt.verify(sessionToken, constants.JWT_SECRET, (err, decoded) => {
            const decodedId = decoded.id
            if (decodedId) {
                User.findOne({ _id: decodedId }).then(
                    (user) => {
                        req['user'] = user;
                        next();
                    },
                    (err) => {
                        res.status(401).send('not authorized: user not found');
                    }
                )
            } else {
                res.status(401).send('not authorized: no decoded id');
            }
        });
    } else {
        next();
    }
};