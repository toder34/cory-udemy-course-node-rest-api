const db = require('../config/db');
const UserSchema = require('./user-schema');

var User = db.model('User', UserSchema);

module.exports = User;