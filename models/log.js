const db = require('../config/db');
const LogSchema = require('./log-schema');
const Log = db.model('Log', LogSchema);

module.exports = Log;